This repo contains slides made with reveal.js
Its automatically deployed to gitlab pages on https://datanauten.gitlab.io/public/slides/matrix-intro/

#Installation

1. clone the repo.
1. update the submodule of reveal.js
1. [install](https://github.com/nvm-sh/nvm#install--update-script) the [node version manager]()
1. install node version 12
1. use node version 12
1. install node packages for reveal.js
1. set symbolic link for reveal.js
1. set symbolic link for all slides
1. run the local server 

```
git clone git@gitlab.com:datanauten/public/slides.git && cd slides
git submodule update --init reveal.js
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
nvm install 12
nvm use 12
cd reveal.js && npm install
ln -s ../reveal.js
ln -s ../matrix-intro
npm start
```

now you can browse the slides at http://localhost:8000/matrix-intro

# PDFs 

You can export PDFs https://datanauten.gitlab.io/public/slides/matrix-intro/?print-pdf#/
https://revealjs.com/pdf-export/


# Themes

the source of a stylesheet needs to be included in /reveal.js/css/theme/source/ folder
that can be done by making a symbolic link
```
cd slides
ln -s matrix-intro/theme/white-dtn10.scss reveal.js/css/theme/source/
```
that need to be build and copy the resulting css into the presentation 
```
cd reveal.js
npm run-script build 
cp dist/theme/white-dtn10.css ../matrix-intro/css
```