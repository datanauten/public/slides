# matrix 4 everyone

#### how to [re]own you communication

---

### Yan Minagawa
 - from Berlin/ Germany
 - Datanaut -> https://datanauten.de/en/about-us/
 - free software advocate
 - works with matrix 
 - loves Nadine
 - and beer ;)
 
---

![External Image](assets/640px-Matrix-logo.svg.png)

Notes: not about the movie
not about matrixprotocol.io a play2earn game coin
---

# [ matrix ]

- open standard protocol for real-time communication

- The Matrix.org Foundation </br> (UK based non-profit Community Interest Company)

- based on the Matrix Manifesto


---
## matrix manifesto

<ul>
<li class="fragment" data-fragment-index="0">
full control over own communication </br> 
<span class="fragment highlight" data-fragment-index="4">-> data sovereignty</span>
</li>

<li class="fragment" data-fragment-index="1">
free to pick host without limiting reach </br> 
<span class="fragment highlight" data-fragment-index="5">-> freedom of choice</span>
</li>

<li class="fragment" data-fragment-index="2">
converse securely and privately is a human right </br> 
<span class="fragment highlight" data-fragment-index="6">-> freedom of speech</span>
</li>

<li class="fragment" data-fragment-index="3">
Communication for everyone: </br> free, open, unencumbered, global </br> 
<span class="fragment highlight" data-fragment-index="7">-> no censorship</span>
</li>

</ul>



Notes:

- People should have full control over their own communication.
- People should not be locked into centralised communication silos, but instead be free to pick who they choose to host their communication without limiting who they can reach.
- The ability to converse securely and privately is a basic human right.
- Communication should be available to everyone as a free and open, unencumbered, standard and global network.

---

## matrix architecture

<img src="assets/distributed_architecture.png" height="450"/>


Notes:
Clients = Application you run on your phone or laptop
the component you interact with
elements, fluffychat, hydrogen, beeper

HomeServers = Client communicates with a server 
keep track of your messages
talk to others servers that other user are using
runs typically in the cloud

It holds user account data, message history, media uploads, room aliases and participates in the Matrix federation network.

VV No single party owns your conversations.
Conversations are shared over all participants. 

Different the all the others ... whatsapp, telegram, skype, discord ...

Application Servers = They provide additional functionality to your homeserver. Like Bridges, Aggregation, Moderation etc.

Identity Server = is to validate, store, and answer questions about the identities 
it stores associations of the form “identifier X represents the same user as identifier Y”, where identities may exist on different systems (such as email addresses, phone numbers, Matrix user IDs, etc) 

--
No single party owns your conversations. <br/>
Conversations are shared over all participants.

Notes:
decentralised like a blockchain

---

## Bridges into other services

<img src="assets/bridges.png" height="450"/>




Notes:
Bridges are services that allow communication between Matrix and non-Matrix platforms. 
https://matrix.org/bridges/
https://www.youtube.com/watch?v=LZdlqTQ3PpY


---
<!-- .slide: data-transition="slide-in fade-out" -->

<img src="assets/dms.png" height="450"/>

Notes:

https://xkcd.com/1810/ 
https://medium.com/@ericmigi/the-universal-communication-bus-42dfb9a141ad

---
<!-- .slide: data-transition="fade-in slide-out" -->

<img src="assets/dms-b.jpeg" height="510"/>

Notes:

https://xkcd.com/1810/ 
https://medium.com/@ericmigi/the-universal-communication-bus-42dfb9a141ad

---
Did I mention it is secure? <br/><br/> Your private conversations can be secured by end to end encryption so the server has no idea what you are talking about.

Notes:
multidevice
multimedia

---

## Let's get going!


<ul>
<li class="fragment" data-fragment-index="0">
get or choose a homeserver:  </br> 
<span class="fragment" data-fragment-index="1">
https://joinmatrix.org/servers/ </br> 
https://wiki.asra.gr/en:public_servers</span>
</li>

<li class="fragment" data-fragment-index="2">
choose username  </br> 
<span class="fragment" data-fragment-index="3">
f.e. @yan:datanauten.de</span>
</li>

<li class="fragment" data-fragment-index="4">
Remember to back up your key!  
</li>

<li class="fragment" data-fragment-index="5">
get matrix client on your phone  </br> 
<span class="fragment" data-fragment-index="6">
f.e. element</span>
</li>

</ul>

Notes:
digital home ... prefered YOUR DOMAIN ... run/rent a homeserver

---
# Questions so far?
(before the live demo)
---

# LiveDemo

Notes:
* show room
* space
* widgets
* multiple clients
* bridged room ()

---

### matrix for DAOS </br> (decentralised autonomos organisations)

- own you communication = own you community
- moving stuff is easy
- no one spys on you

Notes: 
Many crypto communities of today rely on Discord. Discord server is not your server & it’s not a server at all. 

Autonomy of a supposedly autonomous organisation using Discord is undermined in two ways - you can't move your stuff anywhere, everything you say & social graph is analyzed by Discord / whoever else they wish)

---

## current news

<ul>

<li class="fragment" data-fragment-index="0">
2022-03-18: </br> Brazil bans telegram
</li>

<li class="fragment" data-fragment-index="1">
2022-03-25: </br> EU law requires Messenger Interoperability
</li>

</ul>



Notes:


* 2022-03-18 
[brazil bans telegram](https://www.nytimes.com/2022/03/18/world/americas/brazil-bans-telegram.html)
* 2022-03-25 
[Interoperability without sacrificing privacy: Matrix and the DMA
](https://matrix.org/blog/2022/03/25/interoperability-without-sacrificing-privacy-matrix-and-the-dma
)


--- 
## current projects by datanauten

- homeserver for daos
- access to rooms by nft
- pay by the second https://sablier.finance/
- transfer transactions (f.e. lightning)

---

## beyond the horizon

- matrix p2p
- matrix Metaverse
- matrix for games
- matrix for web of mobility

--- 

## Thank you :)<br/>

matrix <span style="color:#2a75dd;">@yan:datanauten.de </span> <br/>
email yan@datanauten.de </span><br/>
nomadlist | twitter | github <span style="color:#2a75dd;">@yncyrydybyl </span> <br/>
+49 175 555 05 64<br/>

https://datanauten.de/en/home/<br/>
twitter <span style="color:#2a75dd;">@datanauten</span> <br/>

